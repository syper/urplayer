import QtQuick 2.7
import QtQuick.Controls 2.2
import Ubuntu.Components 1.3
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import QtMultimedia 5.1
import Ubuntu.Content 1.3

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'urplayer.ubuntouchfr'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)

    Page {
        anchors.fill: parent
            
    header: PageHeader {
        id: main
        visible: false
    }

        Rectangle{
            anchors.fill: parent
            color: "black"
                

            Video {
                id: video
                width : parent.width
                height : parent.height
                source: ""
                autoPlay: true
                    
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        barBottom.visible = !barBottom.visible
                    }
                }

                focus: true
                Keys.onSpacePressed: video.playbackState == MediaPlayer.PlayingState ? video.pause() : video.play()
                Keys.onLeftPressed: video.seek(video.position - 5000)
                Keys.onRightPressed: video.seek(video.position + 5000)
            } 

            
            
            Rectangle{
                id: barTop
                anchors.top: video.top
                width: parent.width
                height: units.gu(8)
                color: "transparent"
                visible: barBottom.visible   
                    
                    Row{
                        anchors.fill: parent
                        
                        Text{
                            id: nameVideo
                            anchors.verticalCenter: parent.verticalCenter
                            anchors.left: parent.left
                            anchors.leftMargin: units.gu(3)
                            text: ""
                            color: "white"
                        }
                        
                        Item{
                            id: menuIcon
                            visible: false //todo
                            anchors.right: parent.right
                            width: units.gu(6)
                            height: units.gu(6)
                            Icon{
                                anchors.verticalCenter: parent.verticalCenter
                                anchors.horizontalCenter: parent.horizontalCenter
                                name: "contextual-menu" 
                                width: units.gu(4)
                                height: units.gu(4)
                                color: "white"
                            }
                           MouseArea {
                                anchors.fill: parent
                                onClicked: {
                                    //TODO
                                }
                            }
                        }
                    }
            }
            
            
            Rectangle{
                id: barBottom
                anchors.bottom: video.bottom
                width: parent.width
                height: units.gu(8)
                color: "transparent"
                    
                    Row{
                        anchors.fill: parent
                        Item{
                            id: playIcon
                            width: units.gu(6)
                            height: units.gu(6)
                            Icon{
                                id: playIconImage
                                anchors.verticalCenter: parent.verticalCenter
                                anchors.horizontalCenter: parent.horizontalCenter
                                    name: if(video.source == ""){"document-open"}else{if(video.playbackState == MediaPlayer.PlayingState){"media-playback-pause"}else{"media-playback-start"}} 
                                width: units.gu(4)
                                height: units.gu(4)
                                color: "white"
                            }
                           MouseArea {
                                anchors.fill: parent
                                onClicked: {
                                    if(playIconImage.name == "document-open"){contentPicker.visible = true}else{video.playbackState == video.MediaPlayer.PlayingState ? video.pause() : video.play()}
                                }
                            }
                        }
                        Item{
                            width: parent.width-(playIcon.width+audioIcon.width)
                            height: units.gu(6)  
                            visible: if(video.source == ""){false}else{true} 
                            Slider {
                                id: slideDuration   
                                    
                   
                                
                                function formatValue(v) { 
                                    return Math.floor(v*1000 / 3600000)%60+":"+Math.floor(v*1000 / 60000)%60+":"+Math.floor(v)% 60;
                                }    
                                    
                                width: parent.width
                                minimumValue: 0
                                maximumValue: Math.floor(video.duration / 1000) 
                                value: Math.floor(video.position / 1000) 
                                live: false
                                                                
                                onPressedChanged:
                                    {
                                        if(pressed)
                                        {
                                        }
                                        else
                                        {
                                            video.seek(Math.floor(slideDuration.value * 1000))
                                        }
                                    }
                            }
                            Row{
                                anchors.top: slideDuration.bottom
                                width: parent.width
                                height: units.gu(3)
                                Item{
                                    width: parent.width/2
                                    height: parent.height
                                    Text{
                                        id: positionActual
                                        
                                        property int ps : Math.floor(video.position / 1000) % 60;
                                        property int pm : Math.floor(video.position / 60000) % 60;
                                        property int ph : Math.floor(video.position / 3600000) % 60;         
                                        
                                        anchors.left: parent.left
                                        text:  ph+":"+("0" + pm).slice(-2)+":"+("0" + ps).slice(-2)
                                        color: "white"
                                    }
                                }
                                Item{
                                    width: parent.width/2
                                    height: parent.height
                                    Text{
                                        id: durationTotal
                                            
                                        property int ds : Math.floor(video.duration / 1000) % 60;
                                        property int dm : Math.floor(video.duration / 60000) % 60;
                                        property int dh : Math.floor(video.duration / 3600000) % 60;         
                                        
                                        anchors.right: parent.right
      

                                        text:  dh+":"+("0" + dm).slice(-2)+":"+("0" + ds).slice(-2)
                                        color: "white"
                                    }
                                }
                            }
                        }
                        Item{
                            id: audioIcon
                            width: units.gu(6)
                            height: units.gu(6)
                            visible: false
                            Icon{
                                anchors.verticalCenter: parent.verticalCenter
                                anchors.horizontalCenter: parent.horizontalCenter
                                name: if(video.volume == 0){"audio-volume-muted"}else{"audio-volume-high"}
                                width: units.gu(4)
                                height: units.gu(4)
                                color: "white"
                            }
                            MouseArea {
                                anchors.fill: parent
                                onClicked: {
                                    //TODO
                                }
                            }
                        }                        
                    }
            }

            
                
        }       
        
    }

    ContentPeerPicker {
        id: contentPicker
        anchors.fill: parent
        visible: false
        contentType: ContentType.Videos
        handler: ContentHandler.Source

        onPeerSelected: {
            peer.selectionType = ContentTransfer.Single
            var activeTransfer = peer.request()
            activeTransfer.stateChanged.connect(function() {
                if (picker.activeTransfer.state === ContentTransfer.Charged) {
                    picker.activeTransfer.items[0].url
                }
            })
        }
       

        onCancelPressed: {
            contentPicker.visible = false
        }
    }


    
Connections {
        target: ContentHub

        onImportRequested: {
            var filePath = String(transfer.items[0].url).replace('file://', '')
            video.source = filePath;
            var fileName = filePath.split("/").pop();
            nameVideo.text = fileName
            contentPicker.visible = false
        }
}
    
}
