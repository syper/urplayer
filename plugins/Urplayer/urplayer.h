#ifndef URPLAYER_H
#define URPLAYER_H

#include <QObject>

class Urplayer: public QObject {
    Q_OBJECT

public:
    Urplayer();
    ~Urplayer() = default;

    Q_INVOKABLE void speak();
};

#endif
