#ifndef URPLAYERPLUGIN_H
#define URPLAYERPLUGIN_H

#include <QQmlExtensionPlugin>

class UrplayerPlugin : public QQmlExtensionPlugin {
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QQmlExtensionInterface")

public:
    void registerTypes(const char *uri);
};

#endif
