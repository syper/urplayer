#include <QtQml>
#include <QtQml/QQmlContext>

#include "plugin.h"
#include "urplayer.h"

void UrplayerPlugin::registerTypes(const char *uri) {
    //@uri Urplayer
    qmlRegisterSingletonType<Urplayer>(uri, 1, 0, "Urplayer", [](QQmlEngine*, QJSEngine*) -> QObject* { return new Urplayer; });
}
